import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'pages/home_pages.dart';
import 'pages/sliver_demo.dart';
import 'pages/logo_test_page.dart';
import 'pages/animated_widget.dart';
import 'pages/easing_animation_page.dart';
import 'pages/parenting_animation_page.dart';
import 'pages/transform_animation.dart';
import 'pages/spring_falling_animation.dart';
import 'pages/receipt_login_pages.dart';

void main(){runApp(MyApp());}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp
    ]);

    return MaterialApp(
      routes: <String,WidgetBuilder>{
        '/sliver' : (BuildContext context) => SilverDemo(),
        '/StatefulAnim' : (BuildContext context) => LogoTestPage(),
        '/AnimatedWidget' : (BuildContext context) => LogoApp(),
        '/EasingAnimation' : (BuildContext context) => EasingAnimationPage(),
        '/ParentingAnimation' : (BuildContext context) => ParentingAnimationPage(),
        '/TransformAnimation' : (BuildContext context) => TransformAnimationPage(),
        '/SpringFallingAnimation' : (BuildContext context) => SpringFallingAnimationPage(),
        '/LoginPage' : (BuildContext context) => LoginPage(),
      },
      home : Home(),
    );
  }
}

