import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'package:flutter_app/custom_widgets/logo_left_icon.dart';
import 'package:flutter_app/custom_widgets/logo_right_icon.dart';

class LogoWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(border: Border.all(color: Colors.blueAccent)),
      child: ParentingStateful(),
    );
  }
}

class ParentingStateful extends StatefulWidget {
  @override
  _ParentingStatefulState createState() => _ParentingStatefulState();
}

class _ParentingStatefulState extends State<ParentingStateful>
    with TickerProviderStateMixin {
  Animation growingAnimation;
  Animation animationLeft, animationRight;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animationLeft = Tween(begin: -0.25, end: 0.0).animate(CurvedAnimation(
      parent: controller,
      curve: Curves.easeIn,
    ))
      ..addStatusListener((status) {
//        if (status == AnimationStatus.completed) {
//          controller.reverse();
//        }
//        if (status == AnimationStatus.dismissed) {
//          Navigator.pop(context);
//        }
      });
    animationRight = Tween(begin: 0.0, end: -0.25).animate(CurvedAnimation(
      parent: controller,
      curve: Curves.easeIn,
    ))
      ..addStatusListener((status) {
//        if (status == AnimationStatus.completed) {
//          controller.reverse();
//        }
//        if (status == AnimationStatus.dismissed) {
//          Navigator.pop(context);
//        }
      });
  }

  @override
  Widget build(BuildContext context) {
//    final double width = MediaQuery.of(context).size.width;
    final double width = 500.0;
    controller.forward();
    return AnimatedBuilder(
      animation: controller,
      builder: (BuildContext context, Widget child) {
        return new Container(
            child: new Center(
              child: Stack(
                children: <Widget>[
                  Positioned(

                    top: 10.0,
                    left: 40.0,
                    child: Transform(
                        transform: Matrix4.translationValues(
                          animationLeft.value * width,
                          0.0,
                          0.0,
                        ),
                        child: LogoLeftIcon()),
                  ),

                  Positioned(
                    top: 10.0,
                    right: -140.0,
                    child: Transform(
                        transform: Matrix4.translationValues(
                          animationRight.value * width,
                          0.0,
                          0.0,
                        ),
                        child: LogoRightIcon()),
                  )
                ],
              ),
            ),
        );
      },
    );
  }
}
