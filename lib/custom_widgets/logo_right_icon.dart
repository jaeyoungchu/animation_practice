import 'package:flutter/material.dart';

class LogoRightIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10.0),
        height: 60.0, //j13
        width: 60.0, //j13
        decoration: BoxDecoration(
          color: Colors.cyan,
          shape: BoxShape.circle,
        ),
        child: Center(
          child: Icon(Icons.kitchen,size: 50.0,color: Colors.white,),
        ),
      ),
    );
  }
}
