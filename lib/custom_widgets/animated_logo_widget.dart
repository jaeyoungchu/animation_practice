import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

class AnimatedLogo extends StatefulWidget {
  @override
  _AnimatedLogoState createState() => _AnimatedLogoState();
}

class _AnimatedLogoState extends State<AnimatedLogo> with SingleTickerProviderStateMixin {

  Animation<double> animation;
  AnimationController controller;

  @override
    void initState() {
    super.initState();
    controller = AnimationController(
        duration: const Duration(milliseconds: 1000),vsync: this);

      animation = Tween(begin: 100.0, end: 300.0).animate(controller)
      ..addListener((){
        setState(() {

        });
      });
      controller.forward();
  }

  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10.0),
        height: animation.value,
        width: animation.value,
        decoration: BoxDecoration(
          color: Colors.amber,
          shape: BoxShape.circle,
        ),
        child: Center(
          child: Icon(Icons.access_alarms,size: animation.value/1.4,color: Colors.white,),
        ),
      ),
    );
  }

  dispose() {
    controller.dispose();
    super.dispose();
  }
}

