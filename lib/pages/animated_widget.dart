import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';



class LogoApp extends StatefulWidget {
  _LogoAppState createState() => _LogoAppState();
}

class _LogoAppState extends State<LogoApp> with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;

  initState() {
    super.initState();
    controller = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    animation = Tween(begin: 0.0, end: 300.0).animate(controller);
    controller.forward();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('AnimatedWidget Demo'),),
        body: AnimatedWidgetDemo(animation: animation));
  }

  dispose() {
    controller.dispose();
    super.dispose();
  }
}


class AnimatedWidgetDemo extends AnimatedWidget {
  AnimatedWidgetDemo({Key key,Animation<double> animation}) : super(key : key, listenable : animation);

  @override
  Widget build(BuildContext context) {
    final Animation<double> animation = listenable;
    return Center(
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10.0),
        height: animation.value,
        width: animation.value,
        decoration: BoxDecoration(
          color: Colors.amber,
          shape: BoxShape.circle,
        ),
        child: Center(
          child: Icon(Icons.access_alarms,size: animation.value/1.4,color: Colors.white,),
        ),
      ),
    );
  }
}