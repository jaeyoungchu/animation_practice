import 'package:flutter/material.dart';
import 'package:flutter_app/custom_widgets/animated_logo_widget.dart';


class LogoTestPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('StatefulAnim'),),
      body: Center(
        child: AnimatedLogo(),
      )
    );
  }
}
