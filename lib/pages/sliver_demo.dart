import 'package:flutter/material.dart';

class SilverDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            title: Text('SliverDemo'),
//            floating: true,
            pinned: true,
            expandedHeight: 230.0,
            flexibleSpace: FlexibleSpaceBar(background: Image.network('https://placeimg.com/480/320/any',fit: BoxFit.fill,),),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate((context,i) =>
                Card(
                  child: Container(
                    padding: EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        CircleAvatar(
                          backgroundColor: Colors.transparent,
                          backgroundImage: NetworkImage('http://i.pravatar.cc/300',),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: Text('This is the card content.'),
                        )
                      ],
                    ),
                  ),
                ),
            ),
          ),
        ],
      ),
    );
  }
}

