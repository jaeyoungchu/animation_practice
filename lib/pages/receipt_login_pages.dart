import 'package:flutter/material.dart';
import 'package:flutter_app/custom_widgets/reciepe_logo_widget.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(),
            ),
            Expanded(
              flex: 2,
              child: Padding(
//                padding: const EdgeInsets.symmetric(horizontal: 1.0),
                padding: EdgeInsets.zero,
                child: LogoWidget(),
              ),
            ),
            Expanded(
              flex: 5,
              child: Container(),
            ),
          ],
        ),
      ),
    );
  }
}
