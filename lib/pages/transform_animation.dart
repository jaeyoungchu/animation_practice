import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

class TransformAnimationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Transform Animation Demo'),),
      body: TransformStateful(),
    );
  }
}

class TransformStateful extends StatefulWidget {
  @override
  _TransformStatefulState createState() => _TransformStatefulState();
}

class _TransformStatefulState extends State<TransformStateful> with TickerProviderStateMixin {


  Animation transitionTween,borderRadius;
  AnimationController _controller;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
        duration: const Duration(seconds: 2), vsync: this)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
//          Navigator.pop(context);
        }
      });

    transitionTween = Tween<double>(
      begin: 50.0,
      end: 200.0,
    ).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Curves.ease,
      ),
    );
    borderRadius = BorderRadiusTween(
      begin: BorderRadius.circular(75.0),
      end: BorderRadius.circular(0.0),
    ).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Curves.ease,
      ),
    );
    _controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      builder: (BuildContext context, Widget child) {
        return Scaffold(
            body: new Center(
                child: new Stack(
                  children: <Widget>[
                    new Center(
                        child: Container(
                          width: 200.0,
                          height: 200.0,
                          child: Image.network('https://placeimg.com/480/320/any',fit: BoxFit.fill,),
//                          color: Colors.black12,

                        )),
                    new Center(
                        child: Container(
                          alignment: Alignment.bottomCenter,
                          width: transitionTween.value,
                          height: transitionTween.value,
                          decoration: BoxDecoration(
                            color: Colors.black12,
                            borderRadius: borderRadius.value,
                          ),
                        )),
                  ],
                )));
      },
    );
  }
}

