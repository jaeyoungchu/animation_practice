import 'package:flutter/material.dart';

class Home extends StatelessWidget {

  List<String> pageNameList;

  Home(){
    pageNameList = new List();
    pageNameList.add('sliver');
    pageNameList.add('StatefulAnim');
    pageNameList.add('AnimatedWidget');
    pageNameList.add('EasingAnimation');
    pageNameList.add('ParentingAnimation');
    pageNameList.add('TransformAnimation');
    pageNameList.add('SpringFallingAnimation');
    pageNameList.add('LoginPage');
  }

  @override
  Widget build(BuildContext context) {
    print('${pageNameList[0]}');
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            title: Text('Home'),
            floating: true,
            pinned: true,
            expandedHeight: 200.0,
            flexibleSpace: FlexibleSpaceBar(
              background: Image.network('https://placeimg.com/480/320/any',fit: BoxFit.fill,),
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate((context,i)=>
              Card(
                child: GestureDetector(
                  child: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Text('${pageNameList[i]}'),
                  ),
                  onTap: (){onTapItem(context,i);},
                ),
              ),
              childCount: pageNameList.length,
            ),
          ),
        ],
      ),
    );
  }

  void onTapItem(BuildContext context,int i){
    print('/${pageNameList[i]}');
    Navigator.of(context).pushNamed('/${pageNameList[i]}');

  }

}
